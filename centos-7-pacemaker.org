#+title: How to setup Cluster "Pacemaker" in ~CentOS 7~.
#+author: Shafiqur Rahman
* Create Two Node Servers. 
* Validate Their IP Address.
* Disable Firewall and ~SELinux~. (For Production setup later)
* Installing Pacemaker.
* Configuring Cluster using ~hacluster~.
* Starting and Ending Pacemaker Services .
* Adding Member Node to the Cluster.
* Creating and Starting Cluster.
* Configuring Cluster Resources.
** Virtual IP of the Cluster.
** Apache Services (~httpd~). (Just to test fail over services)
* Create Constrains for Cluster Resources.
* Test Fail Over.
